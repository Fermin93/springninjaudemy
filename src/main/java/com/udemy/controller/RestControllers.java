package com.udemy.controller;

import com.udemy.model.ContactModel;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest")
public class RestControllers {
    @GetMapping("/checkrest")
    public ResponseEntity<ContactModel> checkRest(){
        ContactModel cm = new ContactModel(1, "Ringo", "Jasso", "5559097502", "Estados Unidos");
        return new ResponseEntity<ContactModel>(cm, HttpStatus.OK);
    }
}